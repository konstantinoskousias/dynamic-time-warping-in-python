# README #

Dynamic Time Warping in Python

### What is this repository for? ###

* Dynamic Time Warping in Python (jupyter notebook)
* 1.0

### Covering ... ###

* Correlation schemes
* Calculating time series Euclidean distance 
* DTW (step by step)
* Visualization schemes

### Dataset ###

* Input a dataset 

### Who do I talk to? ###

Konstantinos Kousias - kostas@simula.no